package xmlParser.sax;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import xmlParser.module.AgroServices;
import java.util.ArrayList;
import java.util.List;

public class MyContentHandler extends DefaultHandler {

    private List<AgroServices> agroServicesList;
    AgroServices tempAgro = new AgroServices();
    private boolean isHeader;
    private boolean isAgroServices;
    private boolean isId;
    private boolean isName;
    private boolean isSize;
    private boolean isSalary;
    private boolean isAgrotechcol;

    @Override
    public void startDocument(){
        System.out.println("Start document");
        agroServicesList = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        System.out.println("Start element " +  qName);
        if (qName.equals("header")) {
            System.out.println("Header reading");
            isHeader = true;
        }else if(qName.equals("body")){
            isHeader = false;
        }else if (qName.equals("AGRO_SERVICE")) {
                tempAgro = new AgroServices();
                isAgroServices = true;
            } else if (qName.equals("id") && !isHeader) {
                isId = true;
            } else if (qName.equals("name") && !isHeader) {
                isName = true;
            } else if (qName.equals("size") && !isHeader) {
                isSize = true;
            } else if (qName.equals("salary") && !isHeader) {
                isSalary = true;
            } else if (qName.equals("agrotechcol") && !isHeader) {
                isAgrotechcol = true;
            }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String data = new String(ch,start,length);
        System.out.println("data = " + data);
        if(isId){
            tempAgro.setId(Long.parseLong(data));
        }else if(isName){
            tempAgro.setName(data);
        }else if(isSize){
            tempAgro.setSize(data);
        }else if(isSalary){
            tempAgro.setSalary(data);
        }else if(isAgrotechcol){
            tempAgro.setAgrotechcol(data);
        }
        System.out.println("tempAgro " + tempAgro);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        System.out.println("End element "+ qName);
        if (qName.equals("header")) {
            System.out.println("Header reading");
            isHeader = true;
        }else if(qName.equals("body")){
            isHeader = false;
        } else if (qName.equals("AGRO_SERVICE")) {
                isAgroServices = false;
                agroServicesList.add(tempAgro);
                tempAgro = null;
                System.out.println(agroServicesList);
            } else if (qName.equals("id") && !isHeader) {
                isId = false;
            } else if (qName.equals("name") && !isHeader) {
                isName = false;
            } else if (qName.equals("size") && !isHeader) {
                isSize = false;
            } else if (qName.equals("salary") && !isHeader) {
                isSalary = false;
            } else if (qName.equals("agrotechcol") && !isHeader) {
                isAgrotechcol = false;
            }

    }

    @Override
    public void endDocument() throws SAXException {
        System.out.println("End document");
    }

    public List<AgroServices> getAgroServicesList() {
        return agroServicesList;
    }
}
