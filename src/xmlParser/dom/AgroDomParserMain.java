package xmlParser.dom;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import xmlParser.module.AgroServices;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class AgroDomParserMain {
    public static void main(String[] args) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse("https://sc.opendata.az/file/475");
            NodeList nodeList = document.getElementsByTagName("AGRO_SERVICE");
            List<AgroServices> agroServicesList = new ArrayList<>();

            for (int i = 0; i < nodeList.getLength() ; i++) {
                Element agroServicesElement = (Element) nodeList.item(i);
                AgroServices agroServices = new AgroServices();
                String id = agroServicesElement.getElementsByTagName("id").item(0).getTextContent();
                agroServices.setId(Long.parseLong(id));

                String name = agroServicesElement.getElementsByTagName("name").item(0).getTextContent();
                agroServices.setName(name);
                String size = agroServicesElement.getElementsByTagName("size").item(0).getTextContent();
                agroServices.setSize(size);
                String salary = agroServicesElement.getElementsByTagName("salary").item(0).getTextContent();
                agroServices.setSalary(salary);
                String agrotechcol = agroServicesElement.getElementsByTagName("agrotechcol").item(0).getTextContent();
                agroServices.setAgrotechcol(agrotechcol);

                agroServicesList.add(agroServices);
            }
            int count = 0;
            NodeList salarynodeList = document.getElementsByTagName("salary");
            for (int i = 0; i < salarynodeList.getLength(); i++) {
                count++;
                Element salaryElement = (Element) salarynodeList.item(i);
                String salary = salaryElement.getTextContent();
                System.out.println("salary = " + salary);
            }
            System.out.println("agro services obyektlerinin sayi = " + agroServicesList.size());
            agroServicesList.forEach(System.out::println);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
    }
}
