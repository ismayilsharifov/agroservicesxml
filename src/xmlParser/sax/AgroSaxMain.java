package xmlParser.sax;

import org.xml.sax.SAXException;
import xmlParser.module.AgroServices;
import xmlParser.module.JDBCUtility;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.*;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class AgroSaxMain {
    public static void main(String[] args) {

        String jdbcurl = "jdbc:oracle:thin:@localhost:1521/orcl";
        String user = "user";
        String password = "****";
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int counter = 0;

        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            SAXParser parser = factory.newSAXParser();
            MyContentHandler handler = new MyContentHandler();
            String url = "https://sc.opendata.az/file/475";
            parser.parse(url,handler);
            List<AgroServices> agroServices = handler.getAgroServicesList();
            agroServices.forEach(System.out::println);

            connection = DriverManager.getConnection(jdbcurl,user,password);
            connection.setAutoCommit(false);
            System.out.println("Bazaya qoshuldu");

            String sql = "insert into agro_services(  service_id,service_name,service_size,salary,agrotechcol)\n" +
                    "values(?,?,?,?,?)";
              ps = connection.prepareStatement(sql);
            ListIterator<AgroServices> agroIter = agroServices.listIterator();
              while(agroIter.hasNext()){
                  AgroServices agro = agroIter.next();
                  ps.setLong(1,agro.getId());
                  ps.setString(2,agro.getName());
                  ps.setString(3,agro.getSize());
                  ps.setString(4,agro.getSalary());
                  ps.setString(5,agro.getAgrotechcol());
                  int count = ps.executeUpdate();
                  counter+=count;
//                  System.out.println(count + " obyekt elave edildi");
              }
            System.out.println("cemi " + counter + " obyekt bazaya elave olundu");
                connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            try {
               if(connection!=null)
                   connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
        e.printStackTrace();
    }
        finally {
            JDBCUtility.close(rs,ps,connection);

        }
    }
}
  /*  create table agro_services(
        service_id number,
        service_name varchar2(255),
    service_size varchar2(40),
    salary varchar2(20),
    agrotechcol varchar2(20),
    constraint service_pk primary key (service_id)
); */
