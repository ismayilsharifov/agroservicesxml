package xmlParser.stax;

import xmlParser.module.AgroServices;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class AgroStaxMain {
    public static void main(String[] args) {
        XMLInputFactory factory = XMLInputFactory.newFactory();
        String uri = "https://sc.opendata.az/file/475";
        List<AgroServices>agroServicesList = new ArrayList<>();
        AgroServices tempAgro = new AgroServices() ;
         boolean isHeader = false;
         boolean isAgroServices = false;
         boolean isId = false;
         boolean isName = false;
         boolean isSize = false;
         boolean isSalary = false;
         boolean isAgrotechcol = false;

        try {
            URL url = new URL("https://sc.opendata.az/file/475");
            XMLStreamReader reader = factory.createXMLStreamReader(url.openStream());
            while(reader.hasNext()){
                int elementType = reader.next();
                if(elementType== XMLEvent.START_DOCUMENT){
                    System.out.println("Start document");
                }else if(elementType==XMLEvent.START_ELEMENT){
                    String qName = reader.getName().toString();
                    System.out.println("Start element " + qName);
                    if (qName.equals("header")) {
                        System.out.println("Header reading");
                        isHeader = true;
                    }else if(qName.equals("body")){
                        isHeader = false;
                    }else if (qName.equals("AGRO_SERVICE")) {
                        tempAgro = new AgroServices();
                        isAgroServices = true;
                    } else if (qName.equals("id") && !isHeader) {
                        isId = true;
                    } else if (qName.equals("name") && !isHeader) {
                        isName = true;
                    } else if (qName.equals("size") && !isHeader) {
                        isSize = true;
                    } else if (qName.equals("salary") && !isHeader) {
                        isSalary = true;
                    } else if (qName.equals("agrotechcol") && !isHeader) {
                        isAgrotechcol = true;
                    }
                }else if(elementType==XMLEvent.CHARACTERS){
                    String data = reader.getText();
                    System.out.println("Data = " + data);
                    if(isId){
                        tempAgro.setId(Long.parseLong(data));
                    }else if(isName){
                        tempAgro.setName(data);
                    }else if(isSize){
                        tempAgro.setSize(data);
                    }else if(isSalary){
                        tempAgro.setSalary(data);
                    }else if(isAgrotechcol){
                        tempAgro.setAgrotechcol(data);
                    }
                    System.out.println("tempAgro " + tempAgro);

                }else if(elementType==XMLEvent.END_ELEMENT){
                    String qName = reader.getName().toString();
                    System.out.println("End element "+ qName);
                    if (qName.equals("header")) {
                        System.out.println("Header reading");
                        isHeader = true;
                    }else if(qName.equals("body")){
                        isHeader = false;
                    } else if (qName.equals("AGRO_SERVICE")) {
                        isAgroServices = false;
                        agroServicesList.add(tempAgro);
                        tempAgro = null;
                        System.out.println(agroServicesList);
                    } else if (qName.equals("id") && !isHeader) {
                        isId = false;
                    } else if (qName.equals("name") && !isHeader) {
                        isName = false;
                    } else if (qName.equals("size") && !isHeader) {
                        isSize = false;
                    } else if (qName.equals("salary") && !isHeader) {
                        isSalary = false;
                    } else if (qName.equals("agrotechcol") && !isHeader) {
                        isAgrotechcol = false;
                    }
                }else if(elementType==XMLEvent.END_DOCUMENT){
                    System.out.println("End document");
                }
            }
            System.out.println("Obyektlerin sayi = " + agroServicesList.size());
            agroServicesList.forEach(System.out::println);
        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
        }
    }
}
