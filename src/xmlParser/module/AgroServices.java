package xmlParser.module;

import java.math.BigDecimal;

public class AgroServices {
    private long id;
    private String name;
    private String size;
    private String salary;
    private String agrotechcol;

    public AgroServices() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }


    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getAgrotechcol() {
        return agrotechcol;
    }

    public void setAgrotechcol(String agrotechcol) {
        this.agrotechcol = agrotechcol;
    }

    @Override
    public String toString() {
        return "AgroServices{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", size='" + size + '\'' +
                ", salary=" + salary +
                ", agrotechcol='" + agrotechcol + '\'' +
                '}';
    }
}
